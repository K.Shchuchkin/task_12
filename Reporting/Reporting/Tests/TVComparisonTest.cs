﻿using Reporting.Pages;
using NUnit.Allure.Core;
using NUnit.Framework;
using NUnit.Allure.Attributes;
using Allure.Commons;
using OpenQA.Selenium;

namespace Reporting.Tests
{
    [TestFixture]
    [AllureNUnit]
    [AllureSuite("TV comparison page test")]
    public class TVComparison
    {
        private TVpage TVPage;
        private ComparisonPage ComparisonPage;
        private Browser Driver { get; set; }
        [OneTimeSetUp]
        public void SetUp()
        {
            Driver = Browser.GetDriverInstance();
            TVPage = new TVpage(Driver);
            Driver.OpenURL("https://catalog.onliner.by/tv");
        }
        [OneTimeTearDown]
        public void TearDown()
        {
            Driver.Quit();
        }
        [Test(Description = "TV comparison page")]
        [AllureSeverity(SeverityLevel.critical)]
        public void TVComparisonPageTest()
        {
            for(int i = 0; i < 2 && i < TVPage.GetNumberOfAvailableElements(); i++)
            {
                TVPage.SelectElementToCompareList(i);
            }
            ComparisonPage = TVPage.ClickOnComparisonPageButton();
            ComparisonPage.MoveToScreenDiagonalSection();
            ComparisonPage.ClickOnTipWindow();
            Assert.True(ComparisonPage.CheckIfTipWindowIsDisplayed());
            ComparisonPage.ClickOnTipWindow();
            Assert.False(ComparisonPage.CheckIfTipWindowIsDisplayed());
            ComparisonPage.RemoveFirstTVFromComparisonList();
        }
    }
}

