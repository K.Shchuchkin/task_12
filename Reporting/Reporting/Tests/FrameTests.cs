using Allure.Commons;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;
using NUnit.Framework;
using OpenQA.Selenium;
using Reporting.Pages;

namespace Reporting.Tests
{
    [TestFixture]
    [AllureNUnit]
    [AllureSuite("Frame test")]
    public class FrameTest
    {
        private MainPage MainPage;
        private JSRunnerIFramePage JSRunnerIFramePage;
        private SandboxFrame SandboxFrame;
        private Browser Driver { get; set; }
        [OneTimeSetUp]
        public void SetUp()
        {
            Driver = Browser.GetDriverInstance();
            MainPage = new MainPage(Driver);
            Driver.OpenURL("http://jsbin.com/?html,output");
        }
        [OneTimeTearDown]
        public void TearDown()
        {
            Driver.Quit();
        }
        [Test(Description = "Frame switching")]
        [AllureSeverity(SeverityLevel.critical)]
        public void FrameSwitchingTest()
        {
            MainPage.SendKeysToActiveInput();
            JSRunnerIFramePage = MainPage.ClickOnFrame();
            Driver.SwitchToFrame(MainPage.Frame);
            SandboxFrame = JSRunnerIFramePage.ClickOnFrame();
            Driver.SwitchToFrame(JSRunnerIFramePage.Frame);
            SandboxFrame.SendKeysToInput();
        }
    }
}

