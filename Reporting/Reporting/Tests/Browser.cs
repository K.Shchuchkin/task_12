﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Reporting.Tests
{
    public class Browser
    {
        private static IWebDriver Driver { get; set; }
        private static Browser DriverInstance;
        public static Browser GetDriverInstance()
        {
            if(DriverInstance == null)
            {
                DriverInstance = new Browser();
            }
            return DriverInstance;
        }
        private Browser()
        {
            Driver = new ChromeDriver($@"{Environment.CurrentDirectory}\Resources");
            Driver.Manage().Timeouts().ImplicitWait = new TimeSpan(0, 0, 30);
            Driver.Manage().Window.Maximize();
        }
        public IWebElement FindElement(By by)
        {
            List<IWebElement> element = Driver.FindElements(by).ToList();
            if (element.Count == 0)
            {
                throw new NoSuchElementException("Cannot find the element.");
            }
            else if (element.Count > 1)
            {
                throw new ArgumentOutOfRangeException("More than one element.");
            }
            return element[0];
        }
        public List<IWebElement> FindElements(By by)
        {
            List<IWebElement> elements = Driver.FindElements(by).ToList();
            if (elements.Count == 0)
            {
                throw new NoSuchElementException("There's no such elements");
            }
            return elements;
        }
        public void MoveToElement(IWebElement element) => new Actions(Driver).MoveToElement(element).Build().Perform();
        public void SendKeys(string text) => new Actions(Driver).SendKeys(text).Build().Perform();
        public void Click(IWebElement element) => new Actions(Driver).Click(element).Build().Perform();
        public void OpenURL(string URL) => Driver.Navigate().GoToUrl(URL);
        public void Close() => Driver.Close();
        public void Quit()
        {
            Driver.Quit();
            DriverInstance = null;
        }
        public IAlert SwitchToAlert()
        {
            IAlert alert;
            try
            {
                alert = Driver.SwitchTo().Alert();
            }
            catch (NoAlertPresentException)
            {
                alert = SwitchToAlert();
            }
            return alert;
        }
        public void SwitchToWindow(string windowHandle) => Driver.SwitchTo().Window(windowHandle);
        public void SwitchToFrame(IWebElement frame)
        {
            MoveToElement(frame);
            Driver.SwitchTo().Frame(frame);
        }
        public string GetCurrentWindowHandle() => Driver.CurrentWindowHandle;
        public List<string> GetWindowHandles() => Driver.WindowHandles.ToList();
        public int GetWindowHandlesCount() => GetWindowHandles().Count;
        public void SwitchToOriginalWindow() => Driver.SwitchTo().Window(Driver.WindowHandles[0]);
        public void WaitUntilElementIsDisplayed(IWebElement element)
        {
            WebDriverWait wait = new WebDriverWait(Driver, new TimeSpan(0, 0, 30));
            wait.Until(Driver => element.Displayed);
        }
        public void WaitUntilElementIsNotDisplayed(IWebElement element)
        {
            WebDriverWait wait = new WebDriverWait(Driver, new TimeSpan(0, 0, 30));
            wait.Until(Driver => !(element.Displayed));
        }
        public void ExecuteScript(string jsScript, IWebElement element)
        {
            ((IJavaScriptExecutor)Driver).ExecuteScript(jsScript, element);
        }
        public void ExecuteScript(string jsScript)
        {
            ((IJavaScriptExecutor)Driver).ExecuteScript(jsScript);
        }
        public string GetWindowHandle(int window)
        {
            return Driver.WindowHandles[window];
        }
    }
}
