using Reporting.Pages;
using NUnit.Framework;
using NUnit.Allure.Core;
using NUnit.Allure.Attributes;
using Allure.Commons;
using System.IO;

namespace Reporting.Tests
{
    [TestFixture]
    [AllureNUnit]
    public class AlertTests
    {
        private AlertsPage AlertPage;
        private const string SendText = "Great site!";
        private Browser Driver { get; set; }
        protected StreamWriter LogFile;
        [OneTimeSetUp]
        public void SetUp()
        {
            LogFile = File.AppendText("LogFile.txt");
            Driver = Browser.GetDriverInstance();
            AlertPage = new AlertsPage(Driver);
            Driver.OpenURL("https://www.toolsqa.com/handling-alerts-using-selenium-webdriver/");
        }
        [Test(Description = "Simple alert")]
        [AllureSeverity(SeverityLevel.critical)]
        public void SimpleAlertTest()
        {
            AlertPage.ClickOnSimpleAlert();
            LogFile.WriteLine(Driver.SwitchToAlert().Text);
            Driver.SwitchToAlert().Accept();
        }
        [Test(Description = "Confirm Popup alert")]
        [AllureSeverity(SeverityLevel.critical)]
        public void ConfirmPopUpTest()
        {
            AlertPage.ClickOnPopUpAlert();
            LogFile.WriteLine(Driver.SwitchToAlert().Text);
            Driver.SwitchToAlert().Accept();
        }
        [Test(Description = "Dismiss Popup alert")]
        [AllureSeverity(SeverityLevel.critical)]
        public void DismissPopUpTest()
        {
            AlertPage.ClickOnPopUpAlert();
            Driver.SwitchToAlert().Dismiss();
        }
        [Test(Description = "Prompt Popup alert")]
        [AllureSeverity(SeverityLevel.critical)]
        public void PromptPopUpTest()
        {
            if (AlertPage.CheckIfCookieBarIsDisplayed())
            {
                AlertPage.CloseCookieBar();
            }
            AlertPage.ClickOnPromptAlert();
            LogFile.WriteLine(Driver.SwitchToAlert().Text);
            Driver.SwitchToAlert().SendKeys(SendText);
            Driver.SwitchToAlert().Accept();
        }
        [OneTimeTearDown]
        public void TearDown()
        {
            Driver.Quit();
            LogFile.Close();
        }
    }
}

