﻿using Reporting.Pages;
using OpenQA.Selenium;
using System.Collections.ObjectModel;
using NUnit.Allure.Core;
using NUnit.Framework;
using System.Diagnostics;
using Allure.Commons;
using NUnit.Allure.Attributes;
using System.IO;
using System.Collections.Generic;

namespace Reporting.Tests
{
    [TestFixture]
    [AllureNUnit]
    [AllureSuite("Store tests")]
    public class StoreTests
    {
        private AppStorePage AppStorePage;
        private PlayStorePage PlayStorePage;
        private TVpage TVpage;
        private const string ExpectedName = "Каталог Onliner";
        protected StreamWriter LogFile;
        private Browser Driver { get; set; }
        [OneTimeSetUp]
        public void SetUp()
        {
            LogFile = File.AppendText("LogFile.txt");
            Driver = Browser.GetDriverInstance();
            TVpage = new TVpage(Driver);
            Driver.OpenURL("https://catalog.onliner.by/tv");
        }
        [OneTimeTearDown]
        public void TearDown()
        {
            LogFile.Close();
            Driver.Quit();
        }
        [Test(Description = "Window switching")]
        [AllureSeverity(SeverityLevel.critical)]
        public void MultiWindowTest()
        {
            AppStorePage = TVpage.ClickOnAppStoreButton();
            PlayStorePage = TVpage.ClickOnPlayStoreButton();
            Assert.True(Driver.GetWindowHandlesCount() == 3);
            string onlinerTVPage = Driver.GetWindowHandle(0);
            string googlePlay = Driver.GetWindowHandle(1);
            string appStore = Driver.GetWindowHandle(2);
            Driver.SwitchToWindow(googlePlay);
            Assert.True(ExpectedName.Contains(PlayStorePage.GetAppNameText()));
            PlayStorePage.ClickOnMoreButton();
            //To Log
            List<IWebElement> similarApps = Driver.FindElements(By.XPath("//div[@class='ZmHEEd ']/*"));
            var watch = new Stopwatch();
            watch.Start();
            while (true)
            {
                try
                {
                    similarApps = Driver.FindElements(By.XPath("//div[@class='ZmHEEd ']/*"));
                    if(similarApps.Count != 0)
                    {
                        break;
                    }
                }
                catch{ }
                if(watch.Elapsed.TotalSeconds >= 5)
                {
                    watch.Stop();
                    break;
                }
            }
            LogFile.WriteLine(similarApps.Count);
            Driver.SwitchToWindow(appStore);
            AppStorePage.ClickOnMoreButton();
            Driver.Close();
            Driver.SwitchToWindow(onlinerTVPage);
            TVpage.ClickOnAdvBanner();
        }
    }
}

