﻿using OpenQA.Selenium;
using Reporting.Tests;

namespace Reporting.Pages
{
    public class AppStorePage: BasePage
    {
        public AppStorePage(Browser webDriver) : base(webDriver)
        {

        }
        #region WebElements
        public IWebElement AppName => Driver.FindElement(By.XPath("//h1[parent::header and contains(@class, 'title')]"));
        public IWebElement MoreButton => Driver.FindElement(By.XPath("//button[ancestor::div[@class='section__description']]"));
        #endregion
        #region Actions
        public void ClickOnMoreButton() => MoreButton.Click();
        #endregion
    }
}
