﻿using OpenQA.Selenium;
using Reporting.Tests;

namespace Reporting.Pages
{
    public class BasePage
    {
        protected Browser Driver;
        public BasePage(Browser webDriver)
        {
            Driver = webDriver;
        }
    }
}
