﻿using OpenQA.Selenium;
using Reporting.Tests;
using System.Collections.Generic;

namespace Reporting.Pages
{
    public class TVpage : BasePage
    {
        public TVpage(Browser webDriver) : base(webDriver)
        {
        }
        #region WebElements
        public List<IWebElement> ToCompareListButton => Driver.FindElements(By.XPath("//div[@class='schema-product__compare' and parent::div[parent::div[not(contains(@class, 'children'))]]]"));
        public IWebElement ComparisonPageButton => Driver.FindElement(By.XPath("//div[@class='compare-button__state compare-button__state_initial']"));
        public IWebElement AppStoreButton => Driver.FindElement(By.XPath("//a[contains(@href, 'itunes')]"));
        public IWebElement GooglePlayButton => Driver.FindElement(By.XPath("//a[contains(@href, 'google') and parent::div[contains(@class, 'filter')]]"));
        public IWebElement AdvFrame => Driver.FindElement(By.XPath("//iframe[ancestor::div[contains(@id, 'adfox_157650090989294157')]]"));
        #endregion
        #region Actions
        public int GetNumberOfAvailableElements() => ToCompareListButton.Count;
        public void SelectElementToCompareList(int i) => ToCompareListButton[i].Click();
        public ComparisonPage ClickOnComparisonPageButton()
        {
            Driver.ExecuteScript("arguments[0].scrollIntoView({block: \"center\"})", ComparisonPageButton);
            ComparisonPageButton.Click();
            return new ComparisonPage(Driver);
        }
        public AppStorePage ClickOnAppStoreButton()
        {
            Driver.ExecuteScript("arguments[0].scrollIntoView({block: \"center\"})", AppStoreButton);
            AppStoreButton.Click();
            return new AppStorePage(Driver);
        }
        public PlayStorePage ClickOnPlayStoreButton() 
        {
            Driver.ExecuteScript("arguments[0].scrollIntoView({block: \"center\"})", GooglePlayButton);
            GooglePlayButton.Click();
            return new PlayStorePage(Driver);
        } 
        public void ClickOnAdvBanner() => AdvFrame.Click();

        #endregion
    }
}
