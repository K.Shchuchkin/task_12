﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using Reporting.Tests;

namespace Reporting.Pages
{
    public class SandboxFrame : BasePage
    {
        public SandboxFrame(Browser webDriver) : base(webDriver)
        {
        }
        #region Web Elements
        public IWebElement Input => Driver.FindElement(By.Id("test"));

        #endregion
        #region Actions
        public void SendKeysToInput()
        {
            Driver.Click(Input);
            Driver.SendKeys("Success!");
        }
        #endregion
    }
}
