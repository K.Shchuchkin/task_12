﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using Reporting.Tests;

namespace Reporting.Pages
{
    public class MainPage : BasePage
    {
        public MainPage(Browser webDriver) : base(webDriver)
        {
        }
        #region Web Elements
        public IWebElement Frame => Driver.FindElement(By.XPath("//iframe[@class='stretch']"));
        public IWebElement ActiveInput => Driver.FindElement(By.XPath("//div[@class='CodeMirror-activeline']"));

        #endregion
        #region Actions
        public void SendKeysToActiveInput() 
        {
            Driver.Click(ActiveInput);
            Driver.SendKeys("<input id=\"test\"/>");
        } 

        public JSRunnerIFramePage ClickOnFrame()
        {
            Frame.Click();
            return new JSRunnerIFramePage(Driver);
        }
        #endregion
    }
}
