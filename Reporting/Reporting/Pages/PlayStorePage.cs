﻿using OpenQA.Selenium;
using Reporting.Tests;

namespace Reporting.Pages
{
    public class PlayStorePage : BasePage
    {
        public PlayStorePage(Browser webDriver) : base(webDriver)
        {
        }
        #region WebElements
        public IWebElement AppName => Driver.FindElement(By.XPath("//span[parent::h1]"));
        public IWebElement MoreButton => Driver.FindElement(By.XPath("//a[parent::div[@class='W9yFB']]"));
        #endregion
        #region Actions
        public string GetAppNameText() => AppName.Text;
        public void ClickOnMoreButton() => MoreButton.Click();
        #endregion
    }
}
