﻿using OpenQA.Selenium;
using Reporting.Tests;

namespace Reporting.Pages
{
    public class AlertsPage : BasePage
    {
        public AlertsPage(Browser webDriver) : base(webDriver)
        {

        }
        #region WebElements
        public IWebElement CookieBar => Driver.FindElement(By.Id("cookie_action_close_header"));
        public IWebElement SimpleAlert => Driver.FindElement(By.XPath("//button[contains(@onclick, 'pushAlert()')]"));
        public IWebElement ConfirmPopUp => Driver.FindElement(By.XPath("//button[following-sibling::span[@id='ConfirmOption']]"));
        public IWebElement PromptPopUp => Driver.FindElement(By.XPath("//button[following-sibling::span[@id='promptOption']]"));
        #endregion
        #region Actions
        public void ClickOnSimpleAlert() => SimpleAlert.Click();
        public void ClickOnPopUpAlert() => ConfirmPopUp.Click();
        public void ClickOnPromptAlert() => PromptPopUp.Click();
        public bool CheckIfCookieBarIsDisplayed() => CookieBar.Displayed;
        public void CloseCookieBar() => CookieBar.Click();
        #endregion
    }
}
