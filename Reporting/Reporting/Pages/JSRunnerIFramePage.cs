﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using Reporting.Tests;

namespace Reporting.Pages
{
    public class JSRunnerIFramePage : BasePage
    {
        public JSRunnerIFramePage(Browser webDriver) : base(webDriver)
        {
        }
        #region Web Elements
        public IWebElement Frame => Driver.FindElement(By.XPath("//iframe[@name='JS Bin Output ']"));
        #endregion
        #region Actions
        public SandboxFrame ClickOnFrame()
        {
            Driver.Click(Frame);
            return new SandboxFrame(Driver);
        }
        #endregion
    }
}
