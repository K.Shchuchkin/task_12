﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using Reporting.Tests;
using System.Collections.Generic;

namespace Reporting.Pages
{
    public class ComparisonPage: BasePage
    {
        public ComparisonPage(Browser webDriver) : base(webDriver)
        {
        }
        #region WebElements
        public IWebElement ScreenDiagonal => Driver.FindElement(By.XPath("//span[text()='Диагональ экрана' and parent::td[not(contains(@class, 'duplicate'))]]"));
        public IWebElement HelpButton => Driver.FindElement(By.XPath("//span[@data-tip-term='Диагональ экрана']"));
        public IWebElement TipWindow => Driver.FindElement(By.XPath("//div[descendant::span[text()='Диагональ экрана'] and @id='productTableTip']"));
        public List<IWebElement> DeleteElement => Driver.FindElements(By.XPath("//a[@title='Удалить' and ancestor::table[@id='product-table']]"));
        #endregion
        #region Actions
        public void MoveToScreenDiagonalSection()
        {
            Driver.ExecuteScript("arguments[0].scrollIntoView({block: \"center\"})", ScreenDiagonal);
            Driver.MoveToElement(ScreenDiagonal);
        }
        public void ClickOnTipWindow()
        {
            HelpButton.Click();
        }
        public bool CheckIfTipWindowIsDisplayed() => TipWindow.Displayed;
        public void RemoveFirstTVFromComparisonList() => DeleteElement[0].Click();
        #endregion
    }
}
