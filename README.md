# 1. Install Allure via Scoop
- 1. Open PowerShell
- 2. Execute `scoop install allure`

> Using NUnit solution

# 2. Packages
* Allure.Commons
* NUnit.Allure

# 3. Allure configuration
- 1. Add `allureConfig.json`
- 2. Set property "Copy to Output Directory" to "Copy if newer"
- 3. Open `allureConfig.json` and paste
```
{
  "allure": {
    "directory": "allure-results",
    "links": [
      "{link}",
      "https://testrail.com/browse/{tms}",
      "https://jira.com/browse/{issue}"
    ]
  }
}
```

# 4. Steps to report generation
- 1. Open .sln file, run tests
- 2. Open bin directory (bin/debug/netcoreapp*.*/)
- 3. Open folder in PowerShell
- 4. Execute 
```
allure generate "allure-results" --clean
allure open "allure-report"
```