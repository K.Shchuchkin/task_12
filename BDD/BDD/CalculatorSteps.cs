﻿
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace BDD
{
    [TestFixture]
    [Binding]
    public class CalculatorSteps
    {
        private int firstNumber;
        private int secondNumber;
        private int result;
        [Given(@"I have entered (.*) into the calculator")]
        public void GivenIHaveEnteredIntoTheCalculator(int p0)
        {
            firstNumber = p0;
        }

        [Given(@"I have also entered (.*) into the calculator")]
        public void GivenIHaveAlsoEnteredIntoTheCalculator(int p0)
        {
            secondNumber = p0;
        }

        [When(@"I press add")]
        public void WhenIPressAdd()
        {
            result = firstNumber + secondNumber;
        }

        [Then(@"the result should be (.*) on the screen")]
        public void ThenTheResultShouldBeOnTheScreen(int p0)
        {
            Assert.AreEqual(result, p0);
        }
    }
}
